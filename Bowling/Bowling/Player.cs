﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    class Player
    {
        public static String name;

        //Constructor of Player
        public Player(string name)
        {
            Player.name = name;
        }

        //Function to get the name of the player.
        public string getName()
        {
            return Player.name;
        }
    }

    
}
