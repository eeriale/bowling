﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Bowling
{
    class Program
    {

        static void Main(string[] args)
        {
            Player player = new Player("John");
            Console.WriteLine("Hello " + player.getName() + "!");

            // List with int array(2 values) one for each throw
            // 2 throws for each round.
            List<int[]> scoreBoard = new List<int[]>();

            //int[] roundResult = new int[2] { 1, 2 };
            //scoreBoard.Add(roundResult);

            // Console.WriteLine("Score: " + scoreBoard[0][1] );

            Random rnd = new Random();
            for (int round = 0; round < 9; round++) //The first 9 rounds.
            {
                Console.WriteLine("====Round " + (round+1) + "========");
                int score1 = rnd.Next(11); // Random values, 0 to 10
                Console.Write("ball1: " + score1);
                if (score1 == 10)
                {
                    Console.WriteLine("\nSTRIKE!!");
                    scoreBoard.Add(new int[2] { 10, 0 });
                    continue; // Go to next round.
                }
                int score2 = 0;
                // The Random function is different if going from 0..
                if (score1 == 0) { score2 = rnd.Next(11); }
                else { score2 = rnd.Next(1, (11-score1)); }
                Console.Write(" | ball2: " + score2 + "\n");
                if ((score1 + score2) == 10)
                {
                    Console.WriteLine("SPARE!!");
                }
                scoreBoard.Add(new int[2] { score1, score2 });
            }

            printScores(scoreBoard, player.getName());


            Console.ReadKey();
        }

        public static void printScores(List<int[]> scores, string name)
        {
            Console.WriteLine("|Round| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10  |");
            Console.WriteLine("-------------------------------------------------");
            Console.Write(    "|"+name+" |");
            for (int i = 0; i < scores.Count; i++)
            {
                string printScore = "";
                int score = (scores[i][0] + scores[i][1]);

                if ((scores[i][0] + scores[i][1]) == 0) { printScore = "- -"; } //zero in ball1 and ball2.
                else if (scores[i][0] == 10){ printScore = "X  "; }             //STRIKE
                else if ((scores[i][0] + scores[i][1]) == 10)
                {                 //SPARE
                    if (scores[i][0] == 0)
                    {
                        printScore += "- /";
                    }
                    else
                    {
                        printScore += scores[i][0] + " /";
                    }
                }
                else
                { // No Strikes or Spares.
                    if (scores[i][0] == 0)
                    {
                        printScore = "- " + scores[i][1];
                    }
                    else if (scores[i][1] == 0)
                    {
                        printScore = scores[i][0] + " -";
                    }
                    else
                    {
                        printScore = scores[i][0] + " " + scores[i][1];
                    }
                }
                Console.Write(printScore + "|");
            }
        }
    }
}
